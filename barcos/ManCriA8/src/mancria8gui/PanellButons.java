package mancria8gui;

import javax.swing.JPanel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagLayout;

import javax.swing.JRadioButton;

import java.awt.GridBagConstraints;

import javax.swing.JButton;

import java.awt.Insets;
import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.SwingConstants;
public class PanellButons extends JPanel implements ActionListener{
	String guardar="Guardar", reset="Reset";
	static boolean rotar = false;
	public PanellButons() {
		setPreferredSize(new Dimension(200, 150));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[] {30, 30};
		gridBagLayout.rowHeights = new int[]{25, 25};
		setLayout(gridBagLayout);
		
		JRadioButton btnRotar = new JRadioButton("rotar");
		btnRotar.setHorizontalAlignment(SwingConstants.CENTER);
		btnRotar.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnRotar.setPreferredSize(new Dimension(150, 25));
		btnRotar.setForeground(new Color(154, 59, 72));
		GridBagConstraints gbc_btnRotar = new GridBagConstraints();
		gbc_btnRotar.gridwidth = 2;
		gbc_btnRotar.fill = GridBagConstraints.BOTH;
		gbc_btnRotar.anchor = GridBagConstraints.WEST;
		gbc_btnRotar.insets = new Insets(0, 0, 5, 5);
		gbc_btnRotar.gridx = 0;
		gbc_btnRotar.gridy = 0;
		add(btnRotar, gbc_btnRotar);
		btnRotar.addActionListener(this);
		
		JButton btnDesa = new JButton("Desa");
		btnDesa.setHorizontalTextPosition(SwingConstants.CENTER);
		btnDesa.setPreferredSize(new Dimension(80, 25));
		btnDesa.setForeground(new Color(154, 59, 72));
		GridBagConstraints gbc_btnDesa = new GridBagConstraints();
		gbc_btnDesa.fill = GridBagConstraints.BOTH;
		gbc_btnDesa.anchor = GridBagConstraints.NORTHWEST;
		gbc_btnDesa.insets = new Insets(0, 0, 5, 5);
		gbc_btnDesa.gridx = 0;
		gbc_btnDesa.gridy = 1;
		add(btnDesa, gbc_btnDesa);
		
		JButton btnReset = new JButton("Reset");
		btnReset.setPreferredSize(new Dimension(80, 25));
		btnReset.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnReset.setForeground(new Color(154, 59, 72));
		GridBagConstraints gbc_btnReset = new GridBagConstraints();
		gbc_btnReset.insets = new Insets(0, 0, 5, 5);
		gbc_btnReset.fill = GridBagConstraints.BOTH;
		gbc_btnReset.anchor = GridBagConstraints.NORTH;
		gbc_btnReset.gridx = 1;
		gbc_btnReset.gridy = 1;
		add(btnReset, gbc_btnReset);
		btnReset.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() instanceof JRadioButton){
			JRadioButton radioButo = (JRadioButton) e.getSource();
			if(radioButo.isSelected()) rotar = true;
			else rotar = false;
		}
		else if(e.getSource() instanceof JButton){
			JButton buto = (JButton) e.getSource();
			if(buto.getText().equals("Reset")){
				PanellDreta.barcos.posarVaixells();
				BattleshipPanell.panellMar.repaint();
			}
		}
	}

}
