package mancria8gui;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class PanellDreta extends JPanel {
	static Barcos barcos;
	PanellButons panellButons;
	public PanellDreta() {
		setPreferredSize(new Dimension(220, 470));
		setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		barcos = new Barcos();
		add(barcos);
		
		panellButons = new PanellButons();
		add(panellButons);
	}

}
