package mancria8gui;

import javax.swing.JPanel;

import java.awt.GridBagLayout;

import javax.swing.JButton;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Color;

public class Barcos extends JPanel{
	JButton barco1, barco2, barco3, barco4, barco5, barco6, barco7, barco8, barco9, barco10;
	PanellMar pM;
	GridBagConstraints gbc_barco1, gbc_barco2, gbc_barco3, gbc_barco4, gbc_barco5, gbc_barco6, gbc_barco7, gbc_barco8, gbc_barco10, gbc_barco9;
	static FocusClass focus;
	public Barcos() {
		setPreferredSize(new Dimension(200, 250));
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{50, 50, 50, 50};
		gridBagLayout.rowHeights = new int[]{50, 50, 50, 50, 50};
		gridBagLayout.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0};
		gridBagLayout.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0};
		setLayout(gridBagLayout);
		focus = new FocusClass();
		
		barco1 = new JButton("1");
		barco1.setBackground(new Color(139, 49, 61));
		barco1.setPreferredSize(new Dimension(50,50));
		gbc_barco1 = new GridBagConstraints();
		gbc_barco1.fill = GridBagConstraints.BOTH;
		gbc_barco1.gridx = 0;
		gbc_barco1.gridy = 0;
		barco1.addFocusListener(focus);
		
		barco2 = new JButton("2");
		barco2.setBackground(new Color(154, 59, 72));
		barco2.setPreferredSize(new Dimension(50,50));
		gbc_barco2 = new GridBagConstraints();
		gbc_barco2.fill = GridBagConstraints.BOTH;
		gbc_barco2.gridx = 1;
		gbc_barco2.gridy = 0;
		barco2.addFocusListener(focus);
		
		barco3 = new JButton("3");
		barco3.setBackground(Color.decode("#A84B57"));
		barco3.setPreferredSize(new Dimension(50,50));
		gbc_barco3 = new GridBagConstraints();
		gbc_barco3.fill = GridBagConstraints.BOTH;
		gbc_barco3.gridx = 2;
		gbc_barco3.gridy = 0;
		barco3.addFocusListener(focus);
		
		barco4 = new JButton("4");
		barco4.setBackground(Color.decode("#B35763"));
		barco4.setPreferredSize(new Dimension(50,50));
		gbc_barco4 = new GridBagConstraints();
		gbc_barco4.fill = GridBagConstraints.BOTH;
		gbc_barco4.gridx = 3;
		gbc_barco4.gridy = 0;
		barco4.addFocusListener(focus);
		barco5 = new JButton("5");
		barco5.setBackground(Color.decode("#BD606C"));
		barco5.setPreferredSize(new Dimension(100,50));
		gbc_barco5 = new GridBagConstraints();
		gbc_barco5.fill = GridBagConstraints.BOTH;
		gbc_barco5.gridwidth = 2;
		gbc_barco5.gridx = 0;
		gbc_barco5.gridy = 1;
		barco5.addFocusListener(focus);
		
		barco6 = new JButton("6");
		barco6.setBackground(Color.decode("#CE6977"));
		barco6.setPreferredSize(new Dimension(100,50));
		gbc_barco6 = new GridBagConstraints();
		gbc_barco6.fill = GridBagConstraints.BOTH;
		gbc_barco6.gridwidth = 2;
		gbc_barco6.gridx = 2;
		gbc_barco6.gridy = 1;
		barco6.addFocusListener(focus);
		
		barco7 = new JButton("7");
		barco7.setBackground(Color.decode("#DF7785"));
		barco7.setPreferredSize(new Dimension(200, 50));
		gbc_barco7 = new GridBagConstraints();
		gbc_barco7.fill = GridBagConstraints.BOTH;
		gbc_barco7.gridwidth = 4;
		gbc_barco7.gridx = 0;
		gbc_barco7.gridy = 2;
		barco7.addFocusListener(focus);
		barco8 = new JButton("8");
		barco8.setBackground(Color.decode("#E8909C"));
		barco8.setPreferredSize(new Dimension(100, 50));
		gbc_barco8 = new GridBagConstraints();
		gbc_barco8.fill = GridBagConstraints.BOTH;
		gbc_barco8.gridwidth = 3;
		gbc_barco8.gridx = 0;
		gbc_barco8.gridy = 3;
		barco8.addFocusListener(focus);
		barco10 = new JButton("10");
		barco10.setBackground(Color.decode("#F6C9CF"));
		barco10.setPreferredSize(new Dimension(50, 100));
		gbc_barco10 = new GridBagConstraints();
		gbc_barco10.fill = GridBagConstraints.BOTH;
		gbc_barco10.gridheight = 2;
		gbc_barco10.gridx = 3;
		gbc_barco10.gridy = 3;
		barco10.addFocusListener(focus);
		barco9 = new JButton("9");
		barco9.setBackground(Color.decode("#EFABB4"));
		barco9.setPreferredSize(new Dimension(100, 50));
		gbc_barco9 = new GridBagConstraints();
		gbc_barco9.fill = GridBagConstraints.BOTH;
		gbc_barco9.gridwidth = 3;
		gbc_barco9.gridx = 0;
		gbc_barco9.gridy = 4;
		barco9.addFocusListener(focus);
		posarVaixells();
	}
	
	public void posarVaixells(){
		if(!(barco1.getParent() instanceof Barcos)){
			barco1.setVisible(false);
			add(barco1, gbc_barco1);
			barco1.setVisible(true);
		}
		if(!(barco2.getParent() instanceof Barcos)){
			barco2.setVisible(false);
			add(barco2, gbc_barco2);
			barco2.setVisible(true);
		}
		if(!(barco3.getParent() instanceof Barcos)){
			barco3.setVisible(false);
			add(barco3, gbc_barco3);
			barco3.setVisible(true);
		}
		if(!(barco4.getParent() instanceof Barcos)){
			barco4.setVisible(false);
			add(barco4, gbc_barco4);
			barco4.setVisible(true);
		}
		if(!(barco5.getParent() instanceof Barcos)){
			barco5.setVisible(false);
			add(barco5, gbc_barco5);
			barco5.setVisible(true);
		}
		if(!(barco6.getParent() instanceof Barcos)){	
			barco6.setVisible(false);
			add(barco6, gbc_barco6);
			barco6.setVisible(true);
		}
		if(!(barco7.getParent() instanceof Barcos)){
			barco7.setVisible(false);
			add(barco7, gbc_barco7);
			barco7.setVisible(true);
		}
		if(!(barco8.getParent() instanceof Barcos)){
			barco8.setVisible(false);
			add(barco8, gbc_barco8);
			barco8.setVisible(true);
		}
		if(!(barco10.getParent() instanceof Barcos)){		
			barco10.setVisible(false);
			add(barco10, gbc_barco10);
			barco10.setVisible(true);
		}
		if(!(barco9.getParent() instanceof Barcos)){
			barco9.setVisible(false);
			add(barco9, gbc_barco9);
			barco9.setVisible(true);
		}
		revalidate();
	}
}
