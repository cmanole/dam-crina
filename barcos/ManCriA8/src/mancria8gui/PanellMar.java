package mancria8gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

public class PanellMar extends JPanel implements MouseListener{
	GestorDistribucions g ;
	GridBagConstraints c;
	String nomFitxer = "partida.hlf";
	GridBagLayout gbl_contentPane;
	int filas=11, columnas=11;
	JLabel m[][] = new JLabel[filas][columnas];
	
	public PanellMar() {
		addMouseListener(this);
		int i,j, t=1;
		char [] letra = {' ','A','B','C','D','E','F','G','H','I','J'};
		setBorder(new EmptyBorder(5, 5, 5, 5));
		setPreferredSize(new Dimension(560,560));
		gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{50,50,50,50,50,50,50,50,50,50,50};
		gbl_contentPane.rowHeights = new int[]{50,50,50,50,50,50,50,50,50,50,50};
		gbl_contentPane.columnWeights = new double[]{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
		gbl_contentPane.rowWeights = new double[]{0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
		setLayout(gbl_contentPane);
		g = new GestorDistribucions();
		c = new GridBagConstraints();
		for(i=0; i<filas; i++)
			for(j=0; j<columnas; j++){
				if(i==0){
					m[i][j]= new JLabel(String.valueOf(letra[j]));
					c.gridy=i;
					c.gridx=j;
					c.fill = GridBagConstraints.BOTH;
					c.weighty = 1.0;
					c.weightx = 1.0;
					m[i][j].setPreferredSize(new Dimension(50,50));
					m[i][j].setHorizontalAlignment(SwingConstants.CENTER);
					m[i][j].setForeground(Color.decode("#0431B4"));
					add(m[i][j], c);
				}
				else if(j==0 && i!=0){
					c.gridy=i;
					c.gridx=j;
					c.fill = GridBagConstraints.BOTH;
					c.weighty = 1.0;
					c.weightx = 1.0;
					c.insets = new Insets(0,0,0,0);
					m[i][j]= new JLabel(String.valueOf(t));
					t++;
					m[i][j].setPreferredSize(new Dimension(50,50));
					m[i][j].setHorizontalAlignment(SwingConstants.CENTER);
					m[i][j].setForeground(Color.decode("#0431B4"));
					add(m[i][j], c);
				}
				else{
					c.gridy=i;
					c.gridx=j;
					m[i][j]= new JLabel(" ");
					m[i][j].setPreferredSize(new Dimension(50,50));
					m[i][j].setForeground(Color.darkGray);
					m[i][j].setBackground(new Color(88,179, 240));
					m[i][j].setOpaque(true);
					m[i][j].setHorizontalAlignment(SwingConstants.CENTER);
					c.insets = new Insets(1, 1, 1, 1);
					c.fill = GridBagConstraints.BOTH;
					c.weighty = 1.0;
					c.weightx = 1.0;
					add(m[i][j], c);
				}
				
			}
	}
	public void mouseClicked(MouseEvent e) {
		//Guardar la posicio del vaixells en un fitxer **********************************
		int[] barcos = g.getCaselles();
		for(int i=0; i<g.getFiles(); i++)
		for(int j=0; j<g.getColumnes(); j++){
			if(m[i+1][j+1].getText().equals(" "))
				barcos[i*g.getColumnes()+j]=0;
			else 
				barcos[i*g.getColumnes()+j]=Integer.parseInt(m[i+1][j+1].getText());
			}
		g.setCaselles(barcos);
		try {
			g.guardar(nomFitxer);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
				
		// Posar els vaixells en el mar ***********************************************************************
		int posicioX = e.getX();
		int posicioY = e.getY();
		JPanel panell = (JPanel) e.getSource();
		GridBagLayout g = (GridBagLayout) panell.getLayout();
		JLabel l = (JLabel) panell.getComponentAt(posicioX, posicioY);
		c.fill = GridBagConstraints.BOTH;
		c.gridx = g.getConstraints(l).gridx;
		c.gridy = g.getConstraints(l).gridy;
		if(c.gridy!=0 && c.gridx!=0){
			JButton b = Barcos.focus.getB();
			JPanel panellBarcos = (JPanel) b.getParent();
			GridBagLayout gBarcos = (GridBagLayout) panellBarcos.getLayout();
			int xButo = gBarcos.getConstraints(b).gridwidth;
			int yButo = gBarcos.getConstraints(b).gridheight;
			if(PanellButons.rotar == true){
				c.gridwidth = yButo;
				c.gridheight = xButo;
			}
			else{
				c.gridwidth = xButo;
				c.gridheight = yButo;
			}
			if(c.gridx+c.gridwidth<=columnas && c.gridy+c.gridheight<=filas){
				b.setVisible(false);
				panell.add(b,c,1);
				b.setVisible(true);
			}
		}		
	}

	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
	}

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
}
