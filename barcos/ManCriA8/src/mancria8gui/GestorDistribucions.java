package mancria8gui;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class GestorDistribucions {
	
	private int files=10; //guarda el nombre de files
	private int columnes=10; //guarda el nombre de columnes
	private int[] caselles; //array que guarda informació de caselles del joc
	FileInputStream fis;
	
	public GestorDistribucions(){
		caselles=new int[files*columnes];
		for(int i=0;i<files*columnes;i++)	
			caselles[i]=0;
	}
	public int getFiles(){
		return files;
	}
	public int getColumnes(){
		return columnes;
	}
	public int [] getCaselles(){
		return caselles;
		
	}
	public void setFiles(int files){
		this.files=files;
		caselles=new int[files*columnes];
		for(int i=0;i<files*columnes;i++)	
			caselles[i]=0;
		
	}
	public void setColumnes(int columnes){
		this.columnes=columnes;
		caselles=new int[files*columnes];
		for(int i=0;i<files*columnes;i++)	
			caselles[i]=0;
		
	}
	public void setCaselles(int [] caselles){
		this.caselles=caselles;
		
	}
	public void guardar(String nomFitxer) throws IOException{
		FileOutputStream fos = new FileOutputStream(nomFitxer);
		
		byte[] b = new byte[127];
		fos.write(0);
		String capcelera = "HLF";
		fos.write(capcelera.getBytes());
		fos.write(1 & 0xFF);
		fos.write((1 >>8) & 0xFF);
		//Seccio informacio
		String info = "INFO";
		fos.write(info.getBytes());
		fos.write(4 & 0xFF);
		fos.write((4 >>8) & 0xFF);
		fos.write(files & 0xFF);
		fos.write((files >>8) & 0xFF);
		fos.write(columnes & 0xFF);
		fos.write((columnes >>8) & 0xFF);
		
		//Seccio distribucio vaixells
		String vaixells = "VAIX";
		fos.write(vaixells.getBytes());
		fos.write(files*columnes & 0xFF);
		fos.write((files*columnes >>8) & 0xFF);
		for(int num: caselles)
			fos.write(num);

		//Seccio fi
		String fi = "_FI_";
		fos.write(fi.getBytes());
		fos.write(0 & 0xFF);
		fos.write((0 >>8) & 0xFF);
		fos.close();
		
	}//split an integer in 2 bytes

	public int [] carregar(String nomFitxer) throws IOException{
		fis = new FileInputStream(nomFitxer);	
		fis.skip(12);
		int petit = fis.read();
		int gran = fis.read();
		int num = petit+gran*256;
		this.setFiles(num);
		this.setColumnes(num);
		fis.skip(6);
		int [] temp = new int[files*columnes];
		for(int i=0; i<files*columnes;i++)
			temp[i]=fis.read();
		fis.close();
		return null;
		
	}	
}
