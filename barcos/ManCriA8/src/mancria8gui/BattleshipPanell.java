package mancria8gui;

import javax.swing.JFrame;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import javax.swing.JPanel;
import java.io.IOException;

public class BattleshipPanell extends JFrame{
	private JPanel contentPane;
	static PanellMar panellMar;
	public static void main(String[] args) throws IOException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BattleshipPanell frame = new BattleshipPanell();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	public BattleshipPanell() {
		contentPane = new JPanel();
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		setContentPane(contentPane);
		setResizable(false);
		
		panellMar = new PanellMar();
		contentPane.add(panellMar);
		
		PanellDreta panellDreta = new PanellDreta();
		contentPane.add(panellDreta);
		pack();
		
	}

}
